# MMIOT docker template

Generic template 

## scripts

Please use [mmiot-core-mw-buildscripts](https://dev.azure.com/mmiot/mmiot-core/_git/mmiot-core-mw-buildscripts) to ensure proper docker toolchain management

### building

```
mmiot-build.sh
```

### running

```
mmiot-run.sh
```

### unit tests

This will run any unit tests using configurations in any folder containing a file named ```docker-compose.utest.yml```.
Unit tests are launched by default by a file called ```/utests.sh``` in the **sut** container.

```
mmiot-itests.sh
```

### integration tests

This will run any unit tests using configurations in any folder containing a file named ```docker-compose.itest.yml```.
Unit tests are launched by the default entrypoint the **tester** module

```
mmiot-utests.sh
```


### pushing

```
mmiot-push.sh
```

# Author

Riccardo Tomasi <riccardo.tomasi@magnetimarelli.com>



