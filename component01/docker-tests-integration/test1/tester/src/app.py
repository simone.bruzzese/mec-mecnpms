#!/usr/bin/python3

import configparser
import logging
import logging.config
import sys
import os
import time

from myutils import myutils

logging.config.fileConfig('logging.properties')

logging.info('tester start')

# File configuration parameters

config = configparser.RawConfigParser()
config.read('app.properties')

# Reading file configuration parameters

conf_aparam = myutils.myutils.str2bool(config.get('General', 'aparam'));

# Reading environment configuration parameters

conf_pyloglevelconsole = os.environ['PYLOGLEVELCONSOLE']
conf_pyloglevelfile = os.environ['PYLOGLEVELFILE']

# Overriding configuration using docker's environment variable

loglevelconsole=eval('logging.' + conf_pyloglevelconsole)
logging.getLogger('consolelogger').setLevel(loglevelconsole)
loglevelfile=eval('logging.' + conf_pyloglevelfile)
logging.getLogger('filelogger').setLevel(loglevelfile)
logging.getLogger().setLevel(min(loglevelfile,loglevelconsole))

# We can start here

logging.info('test 1')
logging.info('test 1 - OK')

logging.info('test 2')
logging.info('test 2 - OK')

#logging.info('test 3')
#logging.warning('test 3 - FAILED')
#sys.exit(1)
	
logging.info('test 4')

import urllib.request
try:
	contents = urllib.request.urlopen("http://mmiot_nginx_1").read()
except Exception as e:
	logging.warning('error on get - nginx is probably missing')
	logging.warning(e)
	logging.warning('test 4 - FAILED')
	sys.exit(1) # anything not zero means errors

logging.warning('test 4 - OK')

sys.exit(0) # 0 means no errors

