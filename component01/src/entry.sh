#!/bin/bash

echo "entry.sh launching. env:"
env

cd /usr/src/app/
sh ./run.sh &

#-f means foreground! This will leave this script running - remember to keep it last
crond -l 8 -f -L /var/log/crond.log

