import datetime
import pytz
import calendar
import sys
import configparser
import logging
import os
import json


class myutils(object):
	
	def __init__(self):
		pass;
	
	@staticmethod
	def jsonpretty(myjson):
		ret = json.dumps(myjson, indent=4, sort_keys=True)
		return ret
		
	
	@staticmethod
	def str2bool(v):
		return v.lower() in ("True", "true", "t", "1", "TRUE")

	@staticmethod
	def loadlist(strlist):
		ret = []
		strlist = strlist.split(',')
		for s in strlist:
			s = s.strip()
			ret.append(s)
		return ret
	
	@staticmethod
	def parsedate(v):
		return datetime.datetime.strptime(v, '%Y-%m-%d %H:%M')

	@staticmethod
	def parsedatem(v):
		return datetime.datetime.strptime(v, '%Y-%m-%d %H:%M:%S.%f')

	@staticmethod
	def convert_to_uct_timestamp(localdate):
		mytz = pytz.timezone('Europe/Rome') 
		localdate = mytz.normalize(mytz.localize(localdate, is_dst=True))
		unixtime = calendar.timegm(localdate.utctimetuple())
		return unixtime
	
	@staticmethod	
	def convert_to_localdate(utctimestamp):
		mytz = pytz.timezone('Europe/Rome') 
		#localdate = mytz.normalize(mytz.localize(localdate, is_dst=True))
		#unixtime = calendar.timegm(localdate.utctimetuple())
		return datetime.datetime.fromtimestamp(utctimestamp)

	@staticmethod	
	def strictly_increasing(L):
		return all(x<y for x, y in zip(L, L[1:]))

	@staticmethod
	def strictly_decreasing(L):
		return all(x>y for x, y in zip(L, L[1:]))	

	@staticmethod
	def read2linesconffile(conffile):
		
		if (not	os.path.isfile(conffile)):
			raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), conf_telegram_bot_key_file) 		

		f = open(conffile, 'r')
		line1 = f.readline().strip()
		line2 = f.readline().strip()
		f.close()
		return line1,line2



