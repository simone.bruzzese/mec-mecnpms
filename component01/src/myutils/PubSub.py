import logging

class PubSub(object):
	
	instance = None
	
	@staticmethod
	def getInstance():
		
		if(PubSub.instance==None):
			PubSub.instance = PubSub()
			
		return PubSub.instance	
	
	def __init__(self):
		self.subscribers = []
	
	def subscribe(self,myself):
		self.subscribers.append(myself)
	

	def send(self,msg):
		for s in self.subscribers:
			s.send(msg)
	
