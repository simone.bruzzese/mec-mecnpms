#!/usr/bin/python3

import configparser
import logging
import logging.config
import sys
import os
import time

from myutils import myutils

logging.config.fileConfig('logging.properties')

logging.info('app start')

# File configuration parameters

config = configparser.RawConfigParser()
config.read('app.properties')

# Reading file configuration parameters

conf_aparam = myutils.myutils.str2bool(config.get('General', 'aparam'));

# Reading environment configuration parameters

conf_pyloglevelconsole = os.environ['PYLOGLEVELCONSOLE']
conf_pyloglevelfile = os.environ['PYLOGLEVELFILE']

# Overriding configuration using docker's environment variable

loglevelconsole=eval('logging.' + conf_pyloglevelconsole)
logging.getLogger('consolelogger').setLevel(loglevelconsole)
loglevelfile=eval('logging.' + conf_pyloglevelfile)
logging.getLogger('filelogger').setLevel(loglevelfile)
logging.getLogger().setLevel(min(loglevelfile,loglevelconsole))

# We can start here

seqno = 0
while(True):
	seqno+=1
	logging.info('tick %i' % seqno)
	time.sleep(5)
	
