#!/bin/sh

# exit 0 means success
# exit <any other value> means fail

DATE=`date +%Y%m%d%H%M%S`

echo "$DATE - Testing start"
exit $1
