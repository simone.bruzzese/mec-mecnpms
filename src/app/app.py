#!/usr/bin/python3

import configparser
import logging
import logging.config
import sys
import os
import time
import json
import string
import random
from flask import Flask, jsonify, abort, make_response, request, url_for, render_template, Response
from flask_cors import CORS, cross_origin
from flask_httpauth import HTTPBasicAuth

logging.config.fileConfig('logging.properties')

logging.info('app start')

# File configuration parameters

config = configparser.RawConfigParser()
config.read('app.properties')

# Overriding configuration using docker's environment variable

conf_pyloglevelconsole = os.environ['PYLOGLEVELCONSOLE']
conf_pyloglevelfile = os.environ['PYLOGLEVELFILE']

loglevelconsole=eval('logging.' + conf_pyloglevelconsole)
logging.getLogger('consolelogger').setLevel(loglevelconsole)
loglevelfile=eval('logging.' + conf_pyloglevelfile)
logging.getLogger('filelogger').setLevel(loglevelfile)
logging.getLogger().setLevel(min(loglevelfile,loglevelconsole))

# Basic Auth
auth = HTTPBasicAuth()

@auth.verify_password
def verify_password(username, password):
	with open("./auth/pwfile.txt", "r") as pwfile:
		for line in pwfile.read().splitlines():
			cred = line.split(":",1)
			if (cred[0] == username and cred[1] == password):
				return True

		return False

### App start and SocketIO init
app = Flask(__name__)
cors = CORS(app,support_credentials=True)

# ALLOW CORS
def custom_response(dictionary):
	content = jsonify(dictionary)
	resp = make_response(content)
	resp.headers['Access-Control-Allow-Origin'] = '*'
	resp.headers['Access-Control-Allow-Methods'] = "POST, GET, OPTIONS, PUT, DELETE, PATCH"
	resp.headers['Access-Control-Allow-Headers'] = "origin, content-type, accept, x-requested-with"
	return resp

# API DOCUMENTATION
@app.route('/api/v1/docs')
@auth.login_required
def index():
	return render_template("index.html")		

### MEASUREMENTS APIs
# GET PING
@app.route('/api/v1/measurement/ping', methods=['GET'])
@auth.login_required
def ping():
	logging.info("ping request")

	if not request.args or not 'ts' in request.args:
		logging.info("No 'ts' parameter provided")
		abort(Response("The mandatory parameter 'ts' was not provided in query string <br> Try with /api/v1/measurement/ping?ts=NUMBER", status=400))

	ts_client = request.args.get('ts')

	return custom_response({'ts':ts_client})

# GET DOWNLINK BANDWIDTH
@app.route('/api/v1/measurement/downlink', methods=['GET'])
@auth.login_required
def downlink():
	logging.info("downlink request")

	if not request.args:
		logging.info("No parameters provided")
		abort(Response("The mandatory parameters 'ts' and 'size' were not provided in query string <br> Try with /api/v1/measurement/downlink?ts=NUMBER&size=NUMBER", status=400))

	elif not 'ts' in request.args:
		logging.info("No 'ts' parameter provided")
		abort(Response("The mandatory parameter 'ts' was not provided in query string <br> Try with /api/v1/measurement/downlink?ts=NUMBER&size=NUMBER", status=400))

	elif not 'size' in request.args:
		logging.info("No 'size' parameter provided")
		abort(Response("The mandatory parameter 'ts' was not provided in query string <br> Try with /api/v1/measurement/downlink?ts=NUMBER&size=NUMBER", status=400))

	ts_client = request.args.get('ts')
	size = int(request.args.get('size'))

	payload = ''.join(random.choices(string.ascii_uppercase + string.digits, k = size))

	return custom_response({'ts': ts_client, "payload": payload})

# GET UPLINK BANDWIDTH
@app.route('/api/v1/measurement/uplink', methods=['POST'])
@auth.login_required
def uplink():
	logging.info("uplink request")

	if not request.args:
		logging.info("No parameters provided")
		abort(Response("The mandatory parameters 'ts' was not provided in query string <br> Try with /api/v1/measurement/uplink?ts=NUMBER", status=400))

	elif not 'ts' in request.args:
		logging.info("No 'ts' parameter provided")
		abort(Response("The mandatory parameter 'ts' was not provided in query string <br> Try with /api/v1/measurement/uplink?ts=NUMBER", status=400))

	if not request.json:
		logging.info("No JSON body provided")
		abort(Response("The mandatory parameter 'payload' was not provided in body <br> Try with {'payload': STRING}", status=400))

	elif not 'payload' in request.json:
		logging.info("No 'payload' provided")
		abort(Response("The mandatory parameter 'payload' was not provided in body <br> Try with {'payload': STRING}", status=400))

	ts_client = request.args.get('ts')
	payload = request.json.get('payload')
	size = len(payload)

	return custom_response({'ts': ts_client, "size": size})

@app.errorhandler(404)
def not_found(error):
	resp = make_response(jsonify({'error': 'Not found'}), 404)
	resp.headers['Access-Control-Allow-Origin'] = '*'
	return resp

if __name__ == '__main__':
	app.run(host='0.0.0.0')
