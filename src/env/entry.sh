#!/bin/bash

echo "entry.sh launching. env:"
env

echo "entry.sh launching speedtest server"
cd /usr/src/speedtest/scripts
sh ./run.sh httpd -DFOREGROUND &

echo "entry.sh launching app"
cd /usr/src/app/
sh ./run.sh &

nginx

#-f means foreground! This will leave this script running - remember to keep it last
crond -l 8 -f -L /var/log/crond.log

