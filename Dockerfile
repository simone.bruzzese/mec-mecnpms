## BUILD SERVER ##

FROM alpine:3.9

LABEL maintainer="simone.bruzzese@external.marelli.com"

#ENV HTTP_PROXY=http://10.0.2.15:8888/
#ENV HTTPS_PROXY=http://10.0.2.15:8888/

# apk packages for all components
RUN apk add python3 py-pip nginx apache2 php7-apache2 php7-openssl

# copying base common scripts
COPY src/utests.sh /

# various apps being here

################################
# Speedtest Server
################################

ENV PORT=8080

COPY src/speedtest/root /usr/src/speedtest

RUN rm -f /var/cache/apk/* && \
mkdir -p /run/apache2/ && \
chown -R apache:apache /usr/src/speedtest/app /var/log/apache2 /run/apache2/ /dev/stdout /dev/stderr && \
chmod -R 755 /usr/src/speedtest/scripts && \
sed -i "s#Listen 80#Listen ${PORT}#g" /etc/apache2/httpd.conf && \
sed -i "s#ServerTokens .*#ServerTokens Prod#g" /etc/apache2/httpd.conf && \
sed -i "s#ServerSignature .*#ServerSignature Off#g" /etc/apache2/httpd.conf && \
sed -i 's#DocumentRoot ".*#DocumentRoot "/usr/src/speedtest/app"#g' /etc/apache2/httpd.conf && \
sed -i 's#<Directory ".*#<Directory "/usr/src/speedtest/app">#g' /etc/apache2/httpd.conf && \
sed -i "s#Options Indexes FollowSymLinks#Options FollowSymLinks#g" /etc/apache2/httpd.conf && \
sed -i "s#AllowOverride None#AllowOverride All#g" /etc/apache2/httpd.conf && \
sed -i "s#ErrorLog logs/error.log#ErrorLog /dev/stderr#g" /etc/apache2/httpd.conf && \
sed -i "s#CustomLog logs/access.log combined#CustomLog /dev/stdout combined#g" /etc/apache2/httpd.conf && \
sed -i "s|#LoadModule rewrite_module modules/mod_rewrite.so|LoadModule rewrite_module modules/mod_rewrite.so|g" /etc/apache2/httpd.conf

################################
# Base environment
################################

RUN mkdir -p /usr/src/env
COPY src/env /usr/src/env/
RUN cat /usr/src/env/cron.d/* >> /var/spool/cron/crontabs/root

################################
# Certs
################################

RUN mkdir -p /usr/src/certs
COPY src/certs /usr/src/certs/

################################
# nginx configurations
################################

COPY src/nginx /etc/nginx
RUN mkdir -p /var/www/localhost 
COPY src/www /var/www/localhost/index

################################
# Python app
################################

RUN mkdir -p /usr/src/app

COPY src/app/requirements.txt /usr/src/app/requirements.txt
RUN cd /usr/src/app/ && pip3 install -r requirements.txt

COPY src/app /usr/src/app/

################################
# Static files
################################

RUN mkdir -p /var/www/localhost/files

COPY src/files /var/www/localhost/files

################################
# that's all folks
################################

CMD ["/bin/sh","/usr/src/env/entry.sh"]
